package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class ArraysMain {

    public static void main(String[] args) {

        Pet[] petArray = new Pet[10];

        Pet firstPet = new Pet();
        firstPet.setName("First com.training.pets.Pet");
        petArray[0] = firstPet;

        Dragon myDragon = new Dragon("Spyro");
        petArray[1] = myDragon;

        for(int i=0; i<petArray.length; ++i) {
            System.out.println(petArray[i]);

            if(petArray[i] != null){
                petArray[i].feed();
            }
        }
    }
}
