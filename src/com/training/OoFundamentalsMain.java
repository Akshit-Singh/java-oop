package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class OoFundamentalsMain {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        System.out.println(Pet.getNumPets());

        myPet.setNumLegs(4);
        myPet.setName("Mycroft");

        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

        Dragon myDragon = new Dragon("Mycroft");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed("goats");
        myDragon.feed();
        myDragon.breatheFire();


        Pet anotherReferenceToDragon;
        anotherReferenceToDragon = myDragon;

        System.out.println(anotherReferenceToDragon.getNumLegs());

        Pet secondDragon = new Dragon();
        secondDragon.setName("Mycroft");

        System.out.println(secondDragon.getNumLegs());
        System.out.println(secondDragon.getName());
        secondDragon.feed();
        ((Dragon)secondDragon).breatheFire();
    }
}
