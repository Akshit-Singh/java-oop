package com.training.pets;

import com.training.Feedable;

public class Dragon extends Pet implements Feedable {

    public Dragon() {
        this.setNumLegs(4);
    }

    public Dragon(String name) {
        this();
        this.setName(name);
    }

    // overloading
    public void feed() {
        System.out.println("Feed dragon some sheep");
    }

    public void feed(String food) {
        System.out.println("Feed dragon some " + food);
    }

    public void breatheFire() {
        System.out.println("FIREE!!!!!");
    }

    @Override
    public String toString() {
        return "This is a dragon, with " + this.getNumLegs() +
                " legs, called " + this.getName();
    }
}
